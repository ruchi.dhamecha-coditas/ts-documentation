// 1. Fixing "X Is Not Assignable To Y"

interface UserProfile {
    id: string;
  
    preferences: {
      type Theme = "light" | "dark" | "blue";
  
    };
  }
  
  let user: UserProfile = {
    id: "123",
    preferences: {
      theme: "blue",
    },
  };

  

  // 2. Fixing "Property Does Not Exist On Type"

  // hovering over `user` shows
const user: {
	name: string;
};
user.age = 24;
const user = {
	name: "Matt",
};




//3. Solving the Possibly Null or Undefined Error



const searchParams = new URLSearchParams(window.location.search);

const id = searchParams.get("id");
if (typeof id==="string") {
	console.log(id.toUpperCase());
}


//4. Fixing "Types Of Property Are Incompatible"
const routingConfig: RoutingConfig = {
	routes: [
		{
			path: "home",
			component: "HomeComponent",
		},
		{
			path: "about",
			component: "AboutComponent",
		},
		{
			path: "contact",
			component: "ContactComponent",
		},
	],
};
type RoutingConfig = {
	routes: {
		path: string;
		component: string;
	}[];
};

const createRoutes = (config: RoutingConfig) => {};

//5. Fixing "X Is Of Type Unknown"

const somethingDangerous = () => {
    if (Math.random() > 0.5) {
      throw new Error('Oh dear!');
    }
  };
  
  try {
    somethingDangerous();
  } catch (error) {
    if (error instanceof Error) {
      console.log(error.message);
    } else {
      throw error;
    }
  }
  

  //6. Fixing "Expression Can't Be Used To Index Type X"

  const productPrices = {
    Apple: 1.2,
    Banana: 0.5,
    Orange: 0.8,
  };
  
  const getPrice = (productName: string) => {
      return productPrices[productName as keyof typeof productPrices];
  };


  //7. Fixing "Property X Has No Initializer"

  class User {
    private username: string; // red squiggly line under username
    constructor(){
        if (Math.random() >0) {
            this.username="";
        }
    }
      }

//8. Fixing "Conversion Of Type X May Be A Mistake"

interface Dog {
    bark: boolean;
  }
  
  let cat = { purr: true };
  
  let dog = cat as any as Dog;


  //9.Fixing "Cannot Redeclare Block-Scoped Variable"
   
  const name = "Matt";
export {};


//10.Fixing "Could Not Find A Declaration File"

npm i --save-dev @types/diff

  