// type annotations means 
// explicitly specify types for identifiers

// for ex. 


//number
const a :number = 10;
const c:number=10.3
let x:number=0XA1   //*******hexadecimal num

//big Integer
let big:bigint = 8907165170179n;



//string
const  b:string = 'avsxj'
const names: string ='ruhci dhameha'
let intro:string = ` im ${names}`


///boolean

let abc :boolean=true
let accepted:boolean=false
let actuve:boolean=true


// object

let employee: {
    firstName:string;
    lastName:string;
    age:number;
    jobTitle:string;
}={
    firstName:'asd',
    lastName:'asda',
    age:25,
    jobTitle:'developer'
};

//array type annotaion

const arr:number[]=[1,2,4]
const arr2:string[]=['absh','sxbaj']
arr2[2]='asnxk'
arr.push(3)


//tuples
let bgColor, headerColor:[number,number,number,number?];
bgColor= [0,255,255,0.5];
headerColor=[0,255,255];


//in function

const funct1 = (num:number)=>num+2

//any

let result: any;
result = 10.123;
result="A";
result=true


//union 

let results: number | string;

//String Literal Types

let mouseEvent:'click'|'dblclick'|'up'|'down';
mouseEvent='click'; //valid
mouseEvent='dblclick';  //valid
mouseEvent='up';   //valid
mouseEvent='down'; //valid
//mouseEvent='mouseover';     //error