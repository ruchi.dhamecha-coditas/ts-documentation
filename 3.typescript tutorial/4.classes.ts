//clases

class Person {
    ssn:string;
    fname:string;
    lname:string;

    constructor(ssn:string,fname:string,lname:string) {
        this.ssn =ssn;
        this.fname =fname;
        this.lname= lname;
    }
    getFullName(): string {
        return `${this.fname} ${this.lname}`;
    }
}

let person = new Person('188','aadsd','cas');


//access modifier 
// private protescted public 

class Person1{
    private ssn:string;
    public name:string;
    protected rollno:string
    // ... 
}


//readonly


class Person2{
     private readonly ssn:string;
    public name:string;
    protected rollno:string
    // ... 
}



//inheritance

class Person3 extends Person1{
    constructor(
        firstName: string,
        lastName: string,
        private jobTitle: string) {
        super(firstName, lastName);
        //super to call constructor of parent 
    }
}
