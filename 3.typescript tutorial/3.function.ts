// functions
let add = function (x:number, y:number) {
    return x + y;
};


//Optional Parameters
let add2 = function (x?:number, y:number) {
    return 2 + y;
};


//Default Parameters
let add3 = function (x:number=3, y:number) {
    return x + y;
};

//Rest Parameters

function getTotal(...numbers: number[]): number {
    let total= 0;
    numbers.forEach((num) => total += num);
    return total;
}
console.log(getTotal(1,2,3)); 
