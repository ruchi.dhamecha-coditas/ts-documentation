interface Person{
    name:string;
    lname:string;
}
let person:Person;

person={
    name:'ashxn',
    lname:'nxk'
}



//extending interfaces


//interface extending 1 interface

interface Mailable{
    send(email:string):boolean
    queue(email:string):boolean
}

interface FutureMailable extends Mailable{
    later(email:string,after:number):boolean
}


//interface extending multiple interfaces
interface A{
    a():void
}
interface B extends A{
    a():void
}
interface C{
    c():void
}
interface D extends B , C{
    d():void
}