// The Implicit ‘Any’ Type Error



import { expect, it } from "vitest";

export const addTwoNumbers = (a:number, b:number) => {
  return a + b;
};

it("Should add the two numbers together", () => {
  expect(addTwoNumbers(2, 4)).toEqual(6);
  expect(addTwoNumbers(10, 10)).toEqual(20);
});



// Working with Object Params

import { expect, it } from "vitest";
type add ={
  first:number,
  second:number
}
export const addTwoNumbers = (params:add) => {
  return params.first + params.second;
};

it("Should add the two numbers together", () => {
  expect(
    addTwoNumbers({
      first: 2,
      second: 4,
    }),
  ).toEqual(6);

  expect(
    addTwoNumbers({
      first: 10,
      second: 20,
    }),
  ).toEqual(30);
});


// Set Properties as Optional

import { expect, it } from 'vitest';

export const getName = (params: { first: string; last?: string }) => {
  if (params.last) {
    return `${params.first} ${params.last}`;
  }
  return params.first;
};

it('Should work with just the first name', () => {
  const name = getName({
    first: 'Matt',
  });

  expect(name).toEqual('Matt');
});

it('Should work with the first and last name', () => {
  const name = getName({
    first: 'Matt',
    last: 'Pocock',
  });

  expect(name).toEqual('Matt Pocock');
});


// 4 Optional Parameters

import { expect, it } from 'vitest';
export const getName = (first: string, last?: string) => {
  if (last) {
    return `${first} ${last}`;
  }
  return first;
};

it('Should work with just the first name', () => {
  const name = getName('Matt');

  expect(name).toEqual('Matt');
});

it('Should work with the first and last name', () => {
  const name = getName('Matt', 'Pocock');

  expect(name).toEqual('Matt Pocock');
});


//5 Assigning Types to Variables

import { expect, it } from 'vitest';

interface User {
  id: number;
  firstName: string;
  lastName: string;
  isAdmin: boolean;
}

/**
 * How do we ensure that defaultUser is of type User
 * at THIS LINE - not further down in the code?
 */

const defaultUser: User = {};

const getUserId = (user: User) => {
  return user.id;
};

it('Should get the user id', () => {
  expect(getUserId(defaultUser)).toEqual(1);
});


// 6 Constraining Value Types

interface User {
  id: number;
  firstName: string;
  lastName: string;
  /**
   * How do we ensure that role is only one of:
   * - 'admin'
   * - 'user'
   * - 'super-admin'
   */
  role: 'admin' | 'user' | 'super-admin';
}

export const defaultUser: User = {
  id: 1,
  firstName: 'Matt',
  lastName: 'Pocock',
  // @ts-expect-error
  role: 'I_SHOULD_NOT_BE_ALLOWED',
};


// 7 Working with Arrays

interface User {
  id: number;
  firstName: string;
  lastName: string;
  role: 'admin' | 'user' | 'super-admin';
  posts: Post;
}

const posts: Post[] = [
  {
    id: 1,
    title: 'autocomplete works',
  },
];

export const defaultUser: User = {
  id: 1,
  firstName: 'Matt',
  lastName: 'Pocock',
  role: 'admin',
  posts: [
    {
      id: 1,
      title: 'How I eat so much cheese',
    },
    {
      id: 2,
      title: "Why I don't eat more vegetables",
    },
  ],
};


// 8 Function Return Type Annotations

import { expect, it } from 'vitest';

interface User {
  id: number;
  firstName: string;
  lastName: string;
  role: 'admin' | 'user' | 'super-admin';
  posts: Array<Post>;
}

interface Post {
  id: number;
  title: string;
}

/**
 * How do we ensure that makeUser ALWAYS
 * returns a user?
 */
const makeUser = (): User => {
  return {};
};

it('Should return a valid user', () => {
  const user = makeUser();

  expect(user.id).toBeTypeOf('number');
  expect(user.firstName).toBeTypeOf('string');
  expect(user.lastName).toBeTypeOf('string');
  expect(user.role).to.be.oneOf(['super-admin', 'admin', 'user']);

  expect(user.posts[0].id).toBeTypeOf('number');
  expect(user.posts[0].title).toBeTypeOf('string');
});


//9 Typing Promises and Async Requests

interface LukeSkywalker {
  name: string;
  height: string;
  mass: string;
  hair_color: string;
  skin_color: string;
  eye_color: string;
  birth_year: string;
  gender: string;
}
export const fetchLukeSkywalker = async () => {
  const data = await fetch('https://swapi.dev/api/people/1').then((res) => {
    return res.json();
  });

  return data;
};


// 10. Passing Type Arguments

import { expect, it } from 'vitest';
import { Equal, Expect } from './helpers/type-utils';

const guitarists = new Set<string>();

guitarists.add('Jimi Hendrix');
guitarists.add('Eric Clapton');

it('Should contain Jimi and Eric', () => {
  expect(guitarists.has('Jimi Hendrix')).toEqual(true);
  expect(guitarists.has('Eric Clapton')).toEqual(true);
});

it('Should give a type error when you try to pass a non-string', () => {
  // @ts-expect-error
  guitarists.add(2);
});

it('Should be typed as an array of strings', () => {
  const guitaristsAsArray = Array.from(guitarists);

  type tests = [Expect<Equal<typeof guitaristsAsArray, string[]>>];
});


//11. Assigning Dynamic Keys to an Object

import { expect, it } from 'vitest';

const createCache = () => {
  const cache: Record<string, string> = {};

  const add = (id: string, value: string) => {
    cache[id] = value;
  };

  const remove = (id: string) => {
    delete cache[id];
  };

  return {
    cache,
    add,
    remove,
  };
};

it('Should add values to the cache', () => {
  const cache = createCache();

  cache.add('123', 'Matt');

  expect(cache.cache['123']).toEqual('Matt');
});

it('Should remove values from the cache', () => {
  const cache = createCache();

  cache.add('123', 'Matt');
  cache.remove('123');

  expect(cache.cache['123']).toEqual(undefined);
});


//12 Narrowing Down Union Types

import { expect, it } from 'vitest';

const coerceAmount = (amount: number | { amount: number }) => {
  if (typeof amount === 'number') {
    return amount;
  }
  return amount.amount;
};

it('Should return the amount when passed an object', () => {
  expect(coerceAmount({ amount: 20 })).toEqual(20);
});

it('Should return the amount when passed a number', () => {
  expect(coerceAmount(20)).toEqual(20);
});


// 13 Typing Errors in a Try-Catch

import { expect, it } from 'vitest';

const tryCatchDemo = (state: 'fail' | 'succeed') => {
  try {
    if (state === 'fail') {
      throw new Error('Failure!');
    }
  } catch (e: any) {
    return e.message;
  }
};

it('Should return the message when it fails', () => {
  expect(tryCatchDemo('fail')).toEqual('Failure!');
});


//14 Inheriting Interface Properties

import { Equal, Expect } from './helpers/type-utils';

/**
 * Here, the id property is shared between all three
 * interfaces. Can you find a way to refactor this to
 * make it more DRY?
 */

interface Base {
  id: string;
}

interface User extends Base {
  firstName: string;
  lastName: string;
}

interface Post extends Base {
  title: string;
  body: string;
}

interface Comment extends Base {
  comment: string;
}

type tests = [
  Expect<Equal<User, { id: string; firstName: string; lastName: string }>>,
  Expect<Equal<Post, { id: string; title: string; body: string }>>,
  Expect<Equal<Comment, { id: string; comment: string }>>
];



//15  Combining Types to Create New Types
interface User {
  id: string;
  firstName: string;
  lastName: string;
}

interface Post {
  id: string;
  title: string;
  body: string;
}

/**
 * How do we type this return statement so it's both
 * User AND { posts: Post[] }
 */
export const getDefaultUserAndPosts = (): User & {posts: Post[]} & {age:number} =>{
  return {
    id: "1",
    firstName: "Matt",
    lastName: "Pocock",
    posts: [
      {
        id: "1",
        title: "How I eat so much cheese",
        body: "It's pretty edam difficult",
      },
    ],
  };
};

const userAndPosts = getDefaultUserAndPosts();

console.log(userAndPosts.posts[0]);

// 16.Selectively Construct Types from Other Types
import { Equal, Expect } from "./helpers/type-utils";

interface User {
  id: string;
  firstName: string;
  lastName: string;
}

/**
 * How do we create a new object type with _only_ the
 * firstName and lastName properties of User?
 */

type MyType = Omit<User,"id">;
type tests = [Expect<Equal<MyType, { firstName: string; lastName: string }>>];


//17 Typing Functions


import { Equal, Expect } from "./helpers/type-utils";

/**
 * How do we type onFocusChange?
 */
 const addListener = (onFocusChange: (isFocused: boolean) =>void)=>{
  window.addEventListener("focus", () => {
    onFocusChange(true);
  });

  window.addEventListener("blur", () => {
    onFocusChange(false);
  });
};


addListener((isFocused) => {
  console.log({ isFocused });

  type tests = [Expect<Equal<typeof isFocused, boolean>>];
});



//18 Typing Async Functions

import { expect, it } from 'vitest';

interface User {
  id: string;
  firstName: string;
  lastName: string;
}

const createThenGetUser = async (
  createUser: ()=>Promise<string>,
  getUser: (id: string) =>Promise<User>
): Promise<User> => {
  const userId: string =await createUser();

  const user = await getUser(userId);

  return user;
};

it('Should create the user, then get them', async () => {
  const user = await createThenGetUser(
    async () => '123',
    async (id) => ({
      id,
      firstName: 'Matt',
      lastName: 'Pocock',
    })
  );

  expect(user).toEqual({
    id: '123',
    firstName: 'Matt',
    lastName: 'Pocock',
  });
});

